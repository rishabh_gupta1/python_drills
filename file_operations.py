"""
Implement the below file operations.
Close the file after each operation.
"""


def read_file(path):
    pass


def write_to_file(path, s):
    pass


def append_to_file(path, s):
    pass


def numbers_and_squares(n, file_path):
    """
    Save the first `n` natural numbers and their squares into a file in the csv format.

    Example file content for `n=3`:

    1,1
    2,4
    3,9
    """
    pass
