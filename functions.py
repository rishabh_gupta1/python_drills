def is_prime(n):
    """
    Check whether a number is prime or not
    """


def n_digit_primes(digit):
    """
    Return a list of `n_digit` primes using the `is_prime` function.

    Set the default value of the `digit` argument to 2
    """
